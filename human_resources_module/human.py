#!/usr/bin/env python3

__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__version__ = '1.0'


class Human:
    def __init__(self, name, surname):
        """ Human class
        :param name: First name
        :param surname: Last name
        """
        self._name = name
        self._surname = surname

    @property
    def name(self):
        return self._name

    @property
    def surname(self):
        return self._surname
