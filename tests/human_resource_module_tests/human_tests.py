#!/usr/bin/env python3

__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__version__ = '1.0'

from human_resources_module.human import Human
import unittest


class HumanTest(unittest.TestCase):
    def test_jan_kowalski(self):
        jan = Human("Jan", "Kowalski")
        self.assertEqual("Jan", jan.name, "There is name other than expected!")
        self.assertEqual("Kowalski", jan.surname, "There is surname other than expected!")

    def test_anna_nowak(self):
        anna = Human("Anna", "Nowak")
        self.assertEqual("Anna", anna.name, "There is name other than expected!")
        self.assertEqual("Nowak", anna.surname, "There is surname other than expected!")


if __name__ == "__main__":
    unittest.main()
